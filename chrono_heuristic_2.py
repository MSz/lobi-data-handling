def create_key(template, outtype=('nii.gz',), annotation_classes=None):
    if template is None or not template:
        raise ValueError('Template must be a valid format string')
    return template, outtype, annotation_classes


def infotodict(seqinfo):
    """Heuristic evaluator for determining which runs belong where

    A few fields are defined by default:

    item: index within category
    subject: participant id
    seqitem: run number during scanning
    subindex: sub index within group
    """

    t1w = create_key('sub-{subject}/anat/sub-{subject}_T1w')
    nback = create_key('sub-{subject}/func/sub-{subject}_task-nback_run-{item:01d}_bold')
    stopsignal = create_key('sub-{subject}/func/sub-{subject}_task-stopsignal_run-{item:01d}_bold')
    fmap_phasediff = create_key('sub-{subject}/fmap/sub-{subject}_phasediff')
    fmap_magnitude = create_key('sub-{subject}/fmap/sub-{subject}_magnitude')

    info = {t1w: [], nback: [], stopsignal: [], fmap_phasediff: [],
            fmap_magnitude: []}

    for idx, s in enumerate(seqinfo):
        # s is a namedtuple with fields equal to the names of the columns
        # found in dicominfo.txt
        if 't1_mpr_iso' in s.series_id:
            info[t1w] = [s.series_id]
        if 'ep2d_bold_stop_signal' in s.protocol_name:
            info[stopsignal].append(s.series_id)
        if 'ep2d_bold_N_back' in s.protocol_name:
            info[nback].append(s.series_id)
        if s.protocol_name == 'gre_field_mapping' and s.image_type[2] == 'M':
                info[fmap_magnitude].append(s.series_id)
        if s.protocol_name == 'gre_field_mapping' and s.image_type[2] == 'P':
                info[fmap_phasediff].append(s.series_id)

    return info
